package com.example.android.materialdesigncodelab;

/**
 * Created by pavan on 9/5/17.
 */
/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import static com.example.android.materialdesigncodelab.Title.titleStrings;

//for the List fragment

public class ListContentFragment2 extends Fragment {

    //--------------------------------------------------------------------------
    // Code for different actions on different fragments

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        // menu.findItem(R.id.action_newItem).setVisible(true);
        //Resources res = getResources();
        //res.getLayout(R.layout.menu_main);
        inflater.inflate(R.menu.list_menu, menu);
        //res.getMenuInflater().inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.list_action_one:
                //fragment = new YourFragment();
                break;
            case R.id.list_action_two:
                // fragment = new AnotherFragment();
                break;
            case R.id.list_action_three:
                // fragment = new AboutFragment();
                break;
        }
        return true;
    }

    //-------------------------------------------------------------------------------


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        RecyclerView recyclerView = (RecyclerView) inflater.inflate(
                R.layout.recycler_view, container, false);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
       // recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(),LinearLayoutManager.VERTICAL));

        SimpleAdapter mAdapter = new SimpleAdapter(recyclerView.getContext(),titleStrings);

        //SimpleAdapter mAdapter = new SimpleAdapter(recyclerView.getContext(),getActivity().getResources().getStringArray(R.array.places));

        List<SimpleSectionedRecyclerViewAdapter.Section> sections =
                new ArrayList<SimpleSectionedRecyclerViewAdapter.Section>();

        //Sections
        sections.add(new SimpleSectionedRecyclerViewAdapter.Section(0,"Section 1"));
        sections.add(new SimpleSectionedRecyclerViewAdapter.Section(2,"Section 2"));
        //sections.add(new SimpleSectionedRecyclerViewAdapter.Section(3,"Section 3"));
        //sections.add(new SimpleSectionedRecyclerViewAdapter.Section(14,"Section 4"));
        //sections.add(new SimpleSectionedRecyclerViewAdapter.Section(20,"Section 5"));

        //Add your adapter to the sectionAdapter
        SimpleSectionedRecyclerViewAdapter.Section[] dummy = new SimpleSectionedRecyclerViewAdapter.Section[sections.size()];

        SimpleSectionedRecyclerViewAdapter mSectionedAdapter = new
                SimpleSectionedRecyclerViewAdapter(recyclerView.getContext(),R.layout.listing_heading,R.id.listing_title,mAdapter);
        mSectionedAdapter.setSections(sections.toArray(dummy));
        mSectionedAdapter.setSections(sections.toArray(dummy));

        //Apply this adapter to the RecyclerView
        recyclerView.setAdapter(mSectionedAdapter);

        return recyclerView;
    }




}
