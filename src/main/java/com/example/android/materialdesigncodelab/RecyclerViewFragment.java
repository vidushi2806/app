package com.example.android.materialdesigncodelab;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import static android.view.View.VISIBLE;


/**
 * Created by Vidushi N on 5/7/2017.
 */

// For the listing fragment

    public class RecyclerViewFragment  extends Fragment implements View.OnTouchListener {

    public static String[] str = new String[1];

    public int LENGTH ;

    //--------------------------------------------------------------------------
    // Code for different actions on different fragments
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }



    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        // menu.findItem(R.id.action_newItem).setVisible(true);
        //Resources res = getResources();
        //res.getLayout(R.layout.menu_main);
        inflater.inflate(R.menu.listing_menu, menu);
        //res.getMenuInflater().inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.listing_action_one:
                //fragment = new YourFragment();
                break;
            case R.id.listing_action_two:
                // fragment = new AnotherFragment();
                break;
            case R.id.listing_action_three:
                // fragment = new AboutFragment();
                break;
        }
        return true;
    }

    //-----------------------------------------------------------------------

    private RecyclerView recyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.swiperecyclerview, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.itemsRecyclerView);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh()
            {
                refreshItems();
            }
        });


            LENGTH = 0;
         String[] mTitle;
         String[] mDesc;
         TypedArray a;
         String[] mNum;

        Resources resources = this.getResources();

        mTitle = resources.getStringArray(R.array.arr_titles);
        mDesc = resources.getStringArray(R.array.arr_desc);
        mNum= resources.getStringArray(R.array.arr_num);
        a = resources.obtainTypedArray(R.array.place_avator);


        EmptyRecyclerViewAdapter mEmptyAdapter =new EmptyRecyclerViewAdapter("No Data Available");
        ContentAdapter adapter = new ContentAdapter(LENGTH,mTitle,mDesc,mNum,a,recyclerView.getContext());

        if (LENGTH == 0)
        {
            recyclerView.setAdapter(mEmptyAdapter);
        }
        else
        {
            recyclerView.setAdapter(adapter);
        }

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return view;


    }

    void refreshItems()
    {
        LENGTH = LENGTH+1;
        String[] mTitle;
        String[] mDesc;
        TypedArray a;
        String[] mNum;

        Resources resources = this.getResources();

        mTitle = resources.getStringArray(R.array.arr_titles);
        mDesc = resources.getStringArray(R.array.arr_desc);
        mNum= resources.getStringArray(R.array.arr_num);
        a = resources.obtainTypedArray(R.array.place_avator);

        ContentAdapter adapter = new ContentAdapter(LENGTH,mTitle,mDesc,mNum,a,recyclerView.getContext());
        recyclerView.setAdapter(adapter);

        onItemsLoadComplete();


    }

    void onItemsLoadComplete()
    {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }





    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView avator;
        public TextView name;
        public TextView description;
        public TextView num;
        public TextView list_none;


        public ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.listing_view, parent, false));
            avator = (ImageView) itemView.findViewById(R.id.listing_avatar);
            name = (TextView) itemView.findViewById(R.id.listing_title);
            description = (TextView) itemView.findViewById(R.id.listing_desc);
            num = (TextView) itemView.findViewById(R.id.listing_num) ;

            //list_none = (TextView) itemView.findViewById(R.id.listing_none) ;
            //itemView.setOnCreateContextMenuListener(this);
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {         //Code for the menu with Delete and Edit option
                    final CharSequence[] items = getResources().getStringArray(R.array.arr_menu);

                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());

                    // builder.setTitle("Select The Action");
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                        }
                    });
                    builder.show();
                    return true;


                }
            });
        }

    }

    /**
     * Adapter to display recycler view.
     */
    public class ContentAdapter extends RecyclerView.Adapter<RecyclerViewFragment.ViewHolder> {
        // Set numbers of List in RecyclerView.
        private  int LENGTH=0;
        private final String[] mTitle;
        private final String[] mDesc;
        private final Drawable[] mAvators;
        private final String[] mNum;
       // private String[] s;

        public ContentAdapter(int len, String[] title,String[] desc, String[] num,TypedArray ab,Context context) {
            Resources resources = context.getResources();
            str= resources.getStringArray(R.array.errors);
            LENGTH = len;
            mTitle = title;
            mDesc = desc;
            mNum= num;
            TypedArray a = ab;
            mAvators = new Drawable[a.length()];
            for (int i = 0; i < mAvators.length; i++) {
                mAvators[i] = a.getDrawable(i);
            }
            a.recycle();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }



        @Override
        public void onBindViewHolder( ViewHolder holder, int position) {

                holder.avator.setImageDrawable(mAvators[position % mAvators.length]);
                holder.name.setText(mTitle[position % mTitle.length]);
                holder.description.setText(mDesc[position % mDesc.length]);
                holder.num.setText(mNum[position % mNum.length]);
       }

        @Override
        public int getItemCount(){
            return LENGTH;
        }
    }

    public class EmptyRecyclerViewAdapter extends RecyclerView.Adapter<EmptyRecyclerViewAdapter.ViewHolder> {

        private String mMessage;

        public EmptyRecyclerViewAdapter(){}

        public EmptyRecyclerViewAdapter(String message){
            mMessage = message;
        }

        @Override
        public EmptyRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.empty_item, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);

            if(mMessage != null){
                viewHolder.mMessageView.setText(mMessage);
            }

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(EmptyRecyclerViewAdapter.ViewHolder holder, int position) {}

        @Override
        public int getItemCount() {
            return 1;//must return one otherwise none item is shown
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView mMessageView;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mMessageView = (TextView) view.findViewById(R.id.empty_item_message);
            }
        }
    }


}
