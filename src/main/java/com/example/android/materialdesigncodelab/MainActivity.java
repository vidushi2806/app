/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.materialdesigncodelab;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static java.security.AccessController.getContext;


/**
 * Provides UI for the main screen.
 */
public class MainActivity extends AppCompatActivity
{

    private DrawerLayout mDrawerLayout;
    TextView one,two,three,g1;
    private static final int MENU_ADD = Menu.FIRST;
    String tag= "MainActivity";
    private ViewPager viewPager;
    private   FloatingActionButton fab;
   // MenuView.ItemView nav_title1, nav_title2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Adding Toolbar to Main screen
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Setting ViewPager for each Tabs
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        // Set Tabs inside Toolbar
        TabLayout tabs = (TabLayout) findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
        // Create Navigation drawer and inlfate layout
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        // Adding menu icon to Toolbar
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            VectorDrawableCompat indicator
                    = VectorDrawableCompat.create(getResources(), R.drawable.ic_menu, getTheme());
            indicator.setTint(ResourcesCompat.getColor(getResources(),R.color.white,getTheme()));
            supportActionBar.setHomeAsUpIndicator(indicator);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }
        // Set behavior of Navigation drawer
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    // This method will trigger on item Click of navigation menu
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // Set item in checked state
                        menuItem.setChecked(true);

                        switch(menuItem.getItemId()) {
                            case R.id.one:
                                viewPager.setCurrentItem(0);
                                Log.d(tag, "Here1");
                                break;
                            case R.id.two:
                                viewPager.setCurrentItem(1);
                                break;
                            case R.id.three:
                                viewPager.setCurrentItem(2);
                                break;
                            case MENU_ADD:
                                Toast.makeText(getApplicationContext(),"Clicked", Toast.LENGTH_SHORT).show();
                                Log.v(tag, "Here");
                                break;
                        }

                        mDrawerLayout.closeDrawers();
                        return true;
                    }
                });


        // Adding Floating Action Button to bottom right of main view
         fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Hello Snackbar!",
                        Snackbar.LENGTH_LONG).show();
            }
        });

        // from here Vidushi
        one=(TextView) MenuItemCompat.getActionView(navigationView.getMenu().
                findItem(R.id.one));
        two=(TextView) MenuItemCompat.getActionView(navigationView.getMenu().
                findItem(R.id.two));
        three=(TextView) MenuItemCompat.getActionView(navigationView.getMenu().
                findItem(R.id.three));

        Menu menu= navigationView.getMenu();
        menu.add(0, MENU_ADD, Menu.NONE, "List 3").setIcon(R.drawable.ic_add);
        //item.setIcon(R.drawable.ic_share);



       // g1= (TextView) MenuItemCompat.getActionView(navigationView.getMenu().
               //findItem(MENU_ADD));


        menu.add(R.id.group_one,2, 2, "Item 2");
        menu.add(R.id.group_two,3, 3, "Item 3");
        menu.add(R.id.group_two,4, 4, "Item 4");
        menu.setGroupCheckable(R.id.group_one, true, true);
        menu.setGroupCheckable(R.id.group_two, true, true);



        //till here
//This method will initialize the count value
        initializeCountDrawer();

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {

            }

            @Override
            public void onPageSelected(int position) {

                switch (position)
                {
                    case 0:
                        fab.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        fab.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        fab.setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        fab.setVisibility(View.GONE);
                        break;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    // this function Vidushi
    private void initializeCountDrawer(){
        //Gravity property aligns the text

        one.setGravity(Gravity.CENTER_VERTICAL);
        one.setTypeface(null, Typeface.BOLD);
        one.setTextColor(Color.rgb(248,94,40));         //changes the color to orange
        one.setText("99+");
        two.setGravity(Gravity.CENTER_VERTICAL);
        two.setTypeface(null,Typeface.BOLD);
        two.setTextColor(Color.rgb(248,94,40));         //changes the color to orange
//count is added
        two.setText("5");
        three.setGravity(Gravity.CENTER_VERTICAL);
        three.setTypeface(null, Typeface.BOLD);
        three.setTextColor(Color.rgb(248,94,40));         //changes the color to orange
        three.setText("10");



    }

    // Add Fragments to Tabs
    private void setupViewPager(ViewPager viewPager) {
        Adapter adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(new ListContentFragment2(), "List");
        adapter.addFragment(new TileContentFragment(), "Tile");
        adapter.addFragment(new CardContentFragment(), "Card");
        adapter.addFragment(new RecyclerViewFragment(), "Listing");
        viewPager.setAdapter(adapter);
    }





    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        //---------------------------------------------------------------------
        // commenting this to remove same actions in every fragment
        //getMenuInflater().inflate(R.menu.menu_main, menu);

        //---------------------------------------------------------------------
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }
        return super.onOptionsItemSelected(item);
    }
}
